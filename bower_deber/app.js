var express = require('express')
var app = express();
var path = require('path')


app.set('views',path.resolve(__dirname,'views') )
app.set('view engine', 'pug')

app.use(express.static(path.join(__dirname,'/public')))


app.get('/', function(req, res) {
  res.render('index')
})

app.listen(3000)
