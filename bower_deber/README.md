## Bower
Bower es un manejador de dependencias pero para el frontend

## Pasos
- Primero se crea un archivo .bowerrc donde se indica donde se guardaran las dependencias que se necesitaran en el proyecto

- Con bower install, prodremos instalar cualquier dependencia que queramos alojados en bower o en github

- Bower automaticamente instalara las dependencias, por ejemplo la dependencia de bootstrap es jquery

- Con bower init, este se creara con una configuracion inicial.

- No olvidar agragar la carpeta donde este ubicados los archivo de bower en el .gitignore
