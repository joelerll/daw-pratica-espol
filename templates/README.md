# Templates engines

## Handlebarsjs
Es derivado de moustache, por ende son completamentes compatibles

| Ventajas                 | Desventajas                   |
|:-------------------------|:------------------------------|
| Custom helpers           | No se puede usar mucha logica |
| compatible con moustache |                               |
| Sintaxis muy limpia      |                               |

```html
  <title>{{title}}</title>
```

## Twig
Originalmente desarrollado para php, pero cuenta con su version para nodejs

| Ventajas               | Desventajas                |
|:-----------------------|:---------------------------|
| Custom tags            | Demasiada logica en vistas |
| Loops muy declarativos |                            |

```html

{{ include('page.html', sandboxed = true) }}
{% autoescape true %}
    {{ var }}
    {{ var|raw }}     {# var won't be escaped #}
    {{ var|escape }}  {# var won't be doubled-escaped #}
{% endautoescape %}

```

## Pug
Es una mejora de jade

| Ventajas               | Desventajas                                    |
|:-----------------------|:-----------------------------------------------|
| Sintaxis muy limpia    | Lleva tiempo aprenderlo                        |
| No hay qye cerrar tags | Un pequeno tiempo de compilacion en el cliente |
| Soporta Markdown       | Sintaxis obliga uso expacion                   |
| Mixins                 |                                                |

```html
- var friends = 10
case friends
  when 0
    p you have no friends
  when 1
    p you have a friend
  default
    p you have #{friends} friends
```


## Conclusion
Ninguno el "el mejor" por decirlo, pero creo que el que mas se puede sacar provecho es Handlebarsjs devido a que nos obliga a colocar poca logica en nuestras vistas y haci previniendo que sea dificil de mantener en el futuro.
Con pug se tendria una sintaxis muy simple y es muy facil de leer, pero es muy estricto en si con su sintaxis.
Con twig podremos colocar mas logica en nustros templates, funciones filtros y otras cosas ,pero con e riesgo de que sea dificil de mantene en el futuri
