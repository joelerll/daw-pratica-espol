var mongoose = require('mongoose')
var Proyectos = mongoose.model('Proyectos')

module.exports.proyectos = function(req, res) {
  Proyectos.find({}, function(err, data) {
    if (err) {
      res.status(404).json({error: 'error en le server'})
    }
    else {
      res.status(200).json(data)
    }
  })
}
