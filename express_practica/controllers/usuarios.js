var mongoose = require('mongoose')
var Usuarios = mongoose.model('Usuarios')

module.exports.buscarUsuarios = function(req, res) {
  Usuarios.find({nombres:  {'$regex': req.query.like, '$options': 'i' }}, function(err, usuarios) {
    res.status(200).json(usuarios)
  })
}
