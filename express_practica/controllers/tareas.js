var mongoose = require('mongoose')
var Tareas = mongoose.model('Tareas')

module.exports.buscarTareas = function(req, res) {
  var bus = req.query.np + ''
  Tareas.find({proyecto: bus}, function(err, proyectos) {
    if (err) {
      res.status(404).json({'error': 'hubo algun error'})
    } else {
      res.status(200).json(proyectos)
    }
  })
}

module.exports.create = function(req, res) {
  Tareas.create({nombre: req.body.nombre, descripcion: req.body.descripcion, estado: req.body.estado, proyecto: req.body.proyecto, responsable: req.body.responsable}, function(err, tarea) {
    if(err) {
      res.status(404).send({'error': 'no se pudo ingresar'})
    } else {
      res.status(200).send(tarea)
    }
  })
}

module.exports.update = function(req, res) {
  var nombre_i = req.params.nombreTarea + ''
  Tareas.update({nombre: req.params.nombreTarea, proyecto: req.params.nombreProyecto},
    {$set: {'estado': req.body.estado}}, function(err, tarea) {
      if(err) {
        res.status(404).json({'error': 'hubo un error al modificar'})
      } else {
        console.log(tarea.nModified)
        if (tarea.nModified) {
          res.json({'modificado': 'exitosamente'})
        } else {
          res.json({'no encontrado': 'no encontrado lo que se quiere modificar'})
        }

      }
    })
}

module.exports.delete = function(req, res) {
  Tareas.where().findOneAndRemove({nombre: req.params.nombreTarea, proyecto: req.params.nombreProyecto}, function(err , ret) {
    if(err) {
      res.status(404).json({'borraro': 'borrado exitosamente'})
    } else {
      res.send({'borrado': 'borrado exitoso'})
    }
  })
}

module.exports.update2 = function(req, res) {
  console.log(req.params.id)
  var nombre_i = req.params.nombreTarea + ''
  Tareas.update({_id: req.params.id},
    {$set: {'estado': req.body.estado}}, function(err, tarea) {
      if(err) {
        res.status(404).json({'error': 'hubo un error al modificar'})
      } else {
        console.log(tarea.nModified)
        if (tarea.nModified) {
          res.json({mensaje: 'exitosamente'})
        } else {
          res.json({mensaje: 'no encontrado lo que se quiere modificar'})
        }

      }
    })
}

module.exports.delete2 = function(req, res) {
  Tareas.where().findOneAndRemove({_id: req.params.id}, function(err , ret) {
    if(err) {
      res.status(404).json({mensaje: 'error'})
    } else {
      res.send({mensaje: 'borrado exitoso'})
    }
  })
}
