import test from 'ava';
var request = require('supertest');
var app = require('../app');
test('foo', t => {
  request(app)
    .get('/api/proyectos')
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function (err, res) {
      t.error(err, 'Ningun error');
      t.same(res.body.length, 0, 'Solo 9 proyectos creados');
      t.fail()
    });
});
