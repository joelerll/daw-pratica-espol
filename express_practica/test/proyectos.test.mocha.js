var supertest = require("supertest");
var chai = require('chai');
var chaiHttp = require('chai-http')
var should = chai.should();
let mongoose = require("mongoose")
let server1 = require('../app')
//let tareas = require('../model/tareas');
var tareas = mongoose.model('Tareas')
var server = supertest.agent("http://localhost:3000");
chai.use(chaiHttp);
describe("Proyectos",function(){
  it("Numero de proyectos",function(done){
    server
    .get("/api/proyectos")
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200)
      res.body.length.should.equal(9)
      done();
    });
  });

});

describe("Tareas",function(){
  it("Match proyecto1 de todas tareas",function(done){
    server
    .get("/api/tareas?np=proyecto1")
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200)
      for(var i=0; i<res.body.length;i++) {
        res.body[i].proyecto.should.equal('proyecto1')
      }
      done();
    });
  });
  it("POST tarea prueba",function(done){
    server
    .post("/api/tareas")
    .send({nombre : 'tareaPrueba', descripcion : 'tarea prueba', estado:'finalizado', proyecto:'proyecto1', responsable:'cedulaPrueba'})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      tareas.remove({nombre: 'tareaPrueba'}, function(err, res) {
        if(!err) {
        }
      })
      done();
    });
  });
  it("DELETE tarea prueba",function(done){
    let tarea = {nombre : 'tareaPruebaDelete', descripcion : 'tarea prueba', estado:'finalizado', proyecto:'proyecto1', responsable:'cedulaPrueba'}
    tareas.create(tarea, function(err, tarea) {
      server
      .delete('/api/tareas/' + tarea._id)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      })
    })
  })
});
