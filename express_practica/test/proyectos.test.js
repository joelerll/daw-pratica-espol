var test = require('tape');
var request = require('supertest');
var app = require('../app');
test('Proyectos todos', function (t) {
  request(app)
    .get('/api/proyectos')
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function (err, res) {
      t.error(err, 'Ningun error');
      t.same(res.body.length, 9, 'Solo 9 proyectos creados');
      t.end();
    });
});
test('GET Tareas por proyecto', function (t) {
  request(app)
    .get('/api/tareas?np=proyecto1')
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function (err, res) {
      t.error(err, 'Ningun error');
      for (var i = 0; i< res.body.length; i++) {
        t.same(res.body[0].proyecto, 'proyecto1', 'Match proyecto en tarea ' + i);
      }
      t.end();
    });
});
