var express = require('express');
var router = express.Router();

var proyectosController = require('../controllers/proyectos')
var tareasController = require('../controllers/tareas')
var usuariosController = require('../controllers/usuarios')

/*proyectos*/
router.get('/proyectos', proyectosController.proyectos)

/*tareas*/
router.get('/tareas', tareasController.buscarTareas)
router.post('/tareas', tareasController.create)
router.put('/tareas/:nombreProyecto/:nombreTarea', tareasController.update)
router.delete('/tareas/:nombreProyecto/:nombreTarea', tareasController.delete)
router.put('/tareas/:id', tareasController.update2)
router.delete('/tareas/:id', tareasController.delete2)
/*usuarios*/
router.get('/usuarios', usuariosController.buscarUsuarios)


module.exports = router;
