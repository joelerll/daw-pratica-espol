angular.module('app',['dndLists','ui.bootstrap']).
  controller('appController', ['$http','$scope','$timeout','$filter','$document','$uibModal','$sce', function($http,$scope, $timeout, $filter,$uibModal,$document,$sce) {
    $http.get('/api/proyectos').then(function(data) {
      $scope.proyectos = data.data;
    })
    $scope.cedulaBorrar = ''
    $scope.getCedulaEscogida = function(index) {
      $scope.cedulaBorrar = index
    }
    $scope.tareas = []
    $scope.tar = [{id: '1'},{id: '2'},{id:'5'}]
    $scope.borrar = function(id){
      //id = $scope.cedulaBorrar
      var cont  = 0
      $scope.models.lists.inicial.forEach(function(conf) {
        if(conf.tarea._id === id) {
          console.log('match')
          $scope.models.lists.inicial.splice(cont,1)
        }
        cont = cont + 1
      })
      cont  = 0
      $scope.models.lists.desarrollo.forEach(function(conf) {
        if(conf.tarea._id === id) {
          $scope.models.lists.desarrollo.splice(cont,1)
        }
        cont = cont + 1
      })
      cont  = 0
      $scope.models.lists.finalizado.forEach(function(conf) {
        if(conf.tarea._id === id) {
          $scope.models.lists.finalizado.splice(cont,1)
        }
        cont = cont + 1
      })

      $http.delete('/api/tareas/'+id).then(function(result) {
      })
    }



    $scope.models = {
         selected: null,
         lists: {"inicial": [], "desarrollo": [], "finalizado": []}
     };

     // Generate initial model
     /*
     for (var i = 1; i <= 3; ++i) {
         $scope.models.lists.inicial.push({label: "Item A" + i});
         $scope.models.lists.desarrollo.push({label: "Item B" + i});
         $scope.models.lists.finalizado.push({label: "Item C" + i});
     }*/

     // Model to JSON for demo purpose
     $scope.$watch('models', function(model) {

         $scope.modelAsJson = angular.toJson(model, true);
         var cont  = 0
         $scope.models.lists.inicial.forEach(function(conf) {
            console.log(conf)
           if(conf.tarea.estado != 'inicial') {
             $http.put('/api/tareas/'+conf.tarea._id,{estado: 'inicial'}).then(function(result) {
              console.log('cambiado a inicial')
             })
              $scope.models.lists.inicial[cont].tarea.estado = 'inicial'
           }

           cont = cont + 1
         })
         cont = 0
         $scope.models.lists.desarrollo.forEach(function(conf) {
           if(conf.tarea.estado != 'desarrollo') {
             $http.put('/api/tareas/'+conf.tarea._id,{estado: 'desarrollo'}).then(function(result) {
              console.log('cambiado a desarrollo')
             })
             $scope.models.lists.desarrollo[cont].tarea.estado = 'desarrollo'
           }
           cont = cont + 1
         })
         cont = 0
         $scope.models.lists.finalizado.forEach(function(conf) {
           if(conf.tarea.estado != 'finalizado') {
             $http.put('/api/tareas/'+conf.tarea._id,{estado: 'finalizado'}).then(function(result) {
              console.log('cambiado a finalizado')
             })
             $scope.models.lists.finalizado[cont].tarea.estado = 'finalizado'
           }
           cont = cont + 1
         })
     }, true);


    $scope.proyecto_selecionado = {}

    $scope.crearTarea = function() {

        $http.post('/api/tareas',$scope.tarea_ingreso).then(function(res) {
          $scope.models.lists.inicial.push({tarea: res.data})
          $scope.tarea_ingreso = {}
        })

    }
    $scope.vacio = false
    $scope.mensaje = ''
    var setTarea = function() {
      $scope.tarea_ingreso = {}
      $scope.tarea_ingreso.nombre = ''
      $scope.tarea_ingreso.descripcion = ''
      $scope.tarea_ingreso.responsable = ''
      $scope.tarea_ingreso.estado = 'inicial'
      $scope.tarea_ingreso.proyecto = $scope.proyecto_selecionado.nombre
      if($scope.tarea_ingreso.nombre == '' || $scope.tarea_ingreso.descripcion == '' || $scope.tarea_ingreso.responsable == '')
      {
        $scope.mensaje = 'no ingrese vacio'
        $scope.vacio = true
      } else {
        $scope.mensaje = ''
        $scope.vacio = false
      }
    }


      $scope.opcion = function() {
        $http.get('/api/tareas?np='+$scope.proyecto_selecionado.nombre).then(function(data) {
          setTarea()
          $scope.tareas = data.data
          $scope.models.lists.inicial = []
            $scope.models.lists.desarrollo = []
            $scope.models.lists.finalizado = []
          $scope.tareas.forEach(function(tarea) {
            if(tarea.estado === 'inicial') {
              $scope.models.lists.inicial.push({tarea: tarea});
            } else if(tarea.estado === 'desarrollo') {
              $scope.models.lists.desarrollo.push({tarea: tarea});
            } else {
              $scope.models.lists.finalizado.push({tarea: tarea});
            }
          })

        })
      }

      $scope.filterIt = function() {
         return $filter('orderBy')($scope.list2, 'title');
      };

      $scope.renderHtml = function(text)
      {
          return $sce.trustAsHtml('<a class="btn btn-danger" href="#" ng-click="borrar(item.tarea._id)" ng-if=\'item.tarea.estado!="finalizado"\'> <i class="fa fa-lg fa-trash-o"  data-toggle="modal" data-target="#myModal"></i></a>');
      };

      $scope.buscarUsuario = function() {

      }
      $scope.$watch('tarea_ingreso', function(v) {
        console.log(v)
      })
      $scope.usuarios_http = {}
      $scope.getUsusarios = function(val) {
        return $http.get('/api/usuarios', {
          params: {
            like: val
          }
        }).then(function(response){
          var misnombres = []
          response.data.forEach(function(n) {
            misnombres.push(n.nombres + ' ' + n.apellidos)
          })
          return misnombres
        });
      };
  }]).
  config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{')
    $interpolateProvider.endSymbol('}]}')
  })
