$(document).ready(function(){

	$.get( "/api/proyectos", function( data ) {
		for(var x = 0; x < data.length; x++){
			$("#sel1").append(
				$("<option>",{"id":data[x].id}).val(data[x].nombre).text(data[x].nombre)
					);
		}
		//Mostrar el proyecto por default?
	});
});

//Esta función se llama cada vez que hay un cambio en el select.
$(document).on('change', 'select', function() {
    //console.log($(this).val());
    $.get("/api/tareas?np=" + $(this).val(), function(data){
		//console.log($(this).val());
		recargarTareas(data);
    })
});


//Función que actualiza los valores de los campos de tareas dado un object con todos los campos.
//Puede ser modulable... but ain't nobody got time for it.
function recargarTareas(objTareas){
	//console.log(objTareas[0].estado);
	$(".task").remove();
	for(var x = 0; x < objTareas.length; x++){
		//console.log(objTareas[x].nombre);
		if(objTareas[x].estado == "inicial"){
			$("#inicio").append(
				$("<p>",{"class":"task text-center", "id":objTareas[x]._id}).text(objTareas[x].nombre));
		}else if(objTareas[x].estado == "desarrollo"){
			$("#desarrollo").append(
				$("<p>",{"class":"task text-center", "id":objTareas[x]._id}).text(objTareas[x].nombre));
		}else {
			$("#finalizado").append(
				$("<p>",{"class":"task text-center", "id":objTareas[x]._id}).text(objTareas[x].nombre));
		}
	}
}

//Cada que estén sobre algún task, éste s epodrá eliminar o editar.
$(document).on('mouseenter', '.task', function() {
    //Se creará los iconos para borrar o editar.
    addIcons(this.id);
});
$(document).on('mouseleave', '.task', function() {
    //do something
    $(".glyph").remove();
});

//Función que crea iconos para borrar o editar.
function addIcons(id){
		$("#" + id).append(
		$("<button>",{"class": "btn btn-link glyph remove"}).append(
		$("<span>",{"class":"glyphicon glyphicon-remove"}
			)
		)
	);
	$("#" + id).append(
		$("<button>",{"class": "btn btn-link glyph edit"}).append(
		$("<span>",{"class":"glyphicon glyphicon-edit "}
			)
		)
	);
}

//Agrega el proyecto seleccionado al modal de ingreso de tareas
$(document).on("click", "#agregarTareasBtn", function(){
	var val = $("#sel1 option:selected").text();
	$("#proyecto").val(val);
});

$(document).on("click", "#btnCrearTarea", function(){
	var nombre = $("#nombre").val();
	var descripcion = $("#descripcion").val();
	var responsable = "Edison";
	var estado = $("#estado").val();
	var proyecto = $("#proyecto").val();
	console.log(nombre);
	console.log(descripcion);
	console.log(responsable);
	console.log(estado);
	console.log(proyecto);
	$.ajax({
		type: "POST",
		url: "/api/tareas",
		data: JSON.stringify({
			"nombre": nombre,
			"descripcion": descripcion,
			"proyecto": proyecto,
			"responsable": responsable,
			"estado": estado
		}),
		contentType: 'application/json',
		success: function(json){
			console.log(json);
		}
	});
})

//<span class="glyphicon glyphicon-remove"></span>

