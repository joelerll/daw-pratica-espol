var mongoose = require('mongoose')

var usuariosSchema = mongoose.Schema({
  nombres: {type: String},
  apellidos: {type: String},
  cedula: {type: String}
})

mongoose.model('Usuarios', usuariosSchema)
