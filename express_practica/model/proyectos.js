var mongoose = require('mongoose')

var proyectosSchema = mongoose.Schema({
  nombre: {type: String}
})

proyectosSchema.pre('save', next => {
  next();
});

mongoose.model('Proyectos', proyectosSchema)
