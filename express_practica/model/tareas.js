var mongoose = require('mongoose')

var tareasSchema = mongoose.Schema({
  nombre: {type: String},
  descripcion: {type: String},
  estado: {type: String},
  proyecto: {type: String}, //nombre proyecto
  responsable: {type: String} //cedula
})

mongoose.model('Tareas', tareasSchema)
