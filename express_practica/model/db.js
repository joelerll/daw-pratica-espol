var mongoose = require('mongoose')
var sleep = require('sleep')
var dbURI = 'mongodb://daw:daw@ds127389.mlab.com:27389/daw'
//var dbURI = 'mongodb://localhost/daw'
mongoose.connect(dbURI)

var db = mongoose.connection

db.on('connected', function() {
  console.log('base de datos conectada')
})

db.on('error', function(err) {
    console.log('Error en conectar mongo ' + err);
});
db.on('disconnected', function() {
    console.log('Mongo desconectado');
});
require('./tareas')
require('./proyectos')
require('./usuarios')
