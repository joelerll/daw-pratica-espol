# Practica express

## Table de api

### Proyectos
| url            | http | response   | descripcion         |     |
|:---------------|:-----|:-----------|:--------------------|:----|
| /api/proyectos | GET  | Json Array | todos los proyectos | [x] |

### Tareas
**Ojo**
Hay dos metodos delete y put, pueden usar cualquiera, pero creo q es mejor el que solo tiene id

| url                                      | http   | response           | descripcion                     | Request           |     |
|:-----------------------------------------|:-------|:-------------------|:--------------------------------|:------------------|:----|
| /api/tareas?np=nombre                    | GET    | Json Array o error | todas las tareas de un proyecto | nada              | [x] |
| /api/tareas                              | POST   | json exito o error | crea nueva tarea                | tarea Objeto json | [x] |
| /api/tareas/:id                          | PUT    | json exito o error | actualiza tarea                 | tarea objeto json | [x] |
| /api/tareas/:id                          | DELETE | json exito o error | borrar tarea                    | nada              | [x] |
| /api/tareas/:nombreProyecto/:nombreTarea | PUT    | json exito o error | actualiza tarea                 | tarea objeto json | [x] |
| /api/tareas/:nombreProyecto/:nombreTarea | DELETE | json exito o error | borrar tarea                    | nada              | [x] |

### Usuarios
| url                       | http | response                     | descripcion        | Request |     |
|:--------------------------|:-----|:-----------------------------|:-------------------|:--------|:----|
| /api/usuarios?like=patron | GET  | Json Array con datos usuario | todas los usuarios | nada    | [x] |
## Database
