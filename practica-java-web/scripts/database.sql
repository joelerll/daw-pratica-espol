create database conferenciasdb;

use conferenciasdb;

create table conferencias (
	id INTEGER AUTO_INCREMENT NOT NULL UNIQUE,
	nombre CHAR(50) NOT NULL,
	fecha CHAR(50),
	descripcion CHAR(200),
	PRIMARY KEY (id)
)ENGINE = InnoDB;

create table asistentes (
	cedula CHAR(50) NOT NULL UNIQUE,
	nombre CHAR(60) NOT NULL,
	apellido CHAR(60) NOT NULL,
	conferencia INTEGER NOT NULL,
	correo CHAR(60),
	PRIMARY KEY (cedula),
	CONSTRAINT FOREIGN KEY (conferencia) REFERENCES conferencias(id)
)ENGINE = InnoDB;


INSERT INTO conferencias (nombre, fecha, descripcion) VALUES 
('prueba1','2016-03-13','prueba descripcion1'),
('prueba2','2012-03-12','prueba descripcion2');

INSERT INTO asistentes (cedula, nombre, apellido, conferencia, correo) VALUES
('0956456465','asistente1','asistente1apellido',1,'joelerll@gmail.com'),
('0956456466','asistente2','asistente2apellido',1,'joelerll2@gmail.com'),
('0956456467','asistente3','asistente3apellido',1,'joelerll3@gmail.com'),
('0956456468','asistente4','asistente4apellido',2,'joelerll@gmail.com'),
('0956456469','asistente5','asistente5apellido',2,'joelerll@gmail.com');