/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores.Asistentes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.AsistenteDAO;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author joelerll
 */
@WebServlet(name = "AsistenteCREATE", urlPatterns = {"/api/AsistenteCREATE"})
public class AsistenteCREATE extends HttpServlet {
    
    private AsistenteDAO asistente = new AsistenteDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader();
        String str = null;
        
        String cedula = "";
        String nombre = "";
        String apellido = "";
        String conferencia = "";
        String correo = "";
        
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        try {
            JSONObject jObj = new JSONObject(sb.toString());
            cedula = jObj.getString("cedula");
            nombre = jObj.getString("nombre");
            apellido = jObj.getString("apellido");
            conferencia = jObj.getString("idConferencia");
            correo = jObj.getString("correo");
        } catch (JSONException ex) {
            System.out.println(ex);
        }
        
        String action = request.getParameter("action");
        Boolean exito = false;
        String json = "";
        JSONObject jsonO      = new JSONObject();
        
        if(action == null) {
            System.out.println("cedula " + cedula);
            System.out.println("nombre " + nombre);
            System.out.println("apellido " + apellido);
            System.out.println("conferencia " + conferencia);
            System.out.println("correo " + correo);
            if (conferencia.equals("?") || cedula.equals("") || nombre.equals("") || apellido.equals("")) {
                try {
                    System.out.println("**************No se pudo insertar primer if***************");
                    json = "{ 'exito': '0', 'msg': 'Errores en la creación' }";
                    jsonO.put("exito", "0");
                    jsonO.put("msg", "Errores en la creación");
                } catch (JSONException ex) {
                    System.out.println(ex);
                }
            } else {
                try {
                    exito = asistente.createAsistente(cedula, nombre, apellido, conferencia, correo);
                } catch (Exception e) {
                    System.out.println("**************No se pudo insertar***************");
                }
                if (exito) {
                    try {
                        System.out.println("Creacion existosa");
                        json = "{ 'exito': '1', 'msg': 'Creación exitosa' }";
                        jsonO.put("exito", "1");
                        jsonO.put("msg", "Creación exitosa");
                    } catch (JSONException ex) {
                        System.out.println(ex);
                    }
                } else {
                    try {
                        System.out.println("Errores");
                        json = "{ 'exito': '0', 'msg': 'Errores en la creación' }";
                        jsonO.put("exito", "0");
                        jsonO.put("msg", "Errores en la creación");
                    } catch (JSONException ex) {
                        System.out.println(ex);
                    }
                }
            }
            System.out.println(json);
            System.out.println(jsonO);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonO.toString());
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
