/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores.Conferencias;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.ConferenciaDAO;
import models.ConferenciaVO;
/**
 *
 * @author joelerll
 */
@WebServlet(name = "ConferenciasControlador", urlPatterns = {"/Conferencias"})
public class ConferenciasControlador extends HttpServlet {
    private ConferenciaDAO conferenciaModelo = new ConferenciaDAO();
    private static String PAGINA = "/conferencias.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        request.setAttribute("conferencias", conferenciaModelo.getConferencias());
//        request.getRequestDispatcher("conferencias.jsp").forward(request, response);
        
        String forward="";
        String action = request.getParameter("action");
        forward = PAGINA;
        
        if(action == null) {
            response.setContentType("text/html;charset=UTF-8");
            request.setAttribute("conferencias", conferenciaModelo.getConferencias());
        } else {
            if (action.equalsIgnoreCase("data")){
                String id = request.getParameter("id");
                ConferenciaVO conferencia = conferenciaModelo.getConferencia(Integer.valueOf(id));
                
                String json = new Gson().toJson(conferencia);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                return;
            }
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        
        if(action != null) {
            if (action.equalsIgnoreCase("delete")){
                String id = request.getParameter("nombre");                
                try {
                  conferenciaModelo.deleteConferencia(id);
                } catch (Exception e) {
                    System.out.println("**************No se pudo insertar***************");
                }
                response.sendRedirect("/practica-java-web/Conferencias");
            } else if (action.equalsIgnoreCase("edit")){
                String id = request.getParameter("id-conferencia");
                String nombre = request.getParameter("nombre");
                String fecha = request.getParameter("fecha");
                String descripcion = request.getParameter("descripcion");
                try {
                  conferenciaModelo.editConferencia(id, nombre, fecha, descripcion);
                } catch (Exception e) {
                    System.out.println("**************No se pudo insertar***************");
                }
                response.sendRedirect("/practica-java-web/Conferencias");
            } else if (action.equalsIgnoreCase("create")) {
                String nombre = request.getParameter("nombre");
                String fecha = request.getParameter("fecha");
                String descripcion = request.getParameter("descripcion");
                try {
                  conferenciaModelo.createConferencia(nombre, fecha, descripcion);
                } catch (Exception e) {
                    System.out.println("**************No se pudo insertar***************");
                }
                response.sendRedirect("/practica-java-web/Conferencias");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
