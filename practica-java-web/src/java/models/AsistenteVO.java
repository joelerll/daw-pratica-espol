package models;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joelerll
 */
public class AsistenteVO {
    public String cedula;
    public String nombre;
    public String apellido;
    public int conferencia_id;
    public String correo;

    public AsistenteVO() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getConferencia_id() {
        return conferencia_id;
    }

    public void setConferencia_id(int conferencia_id) {
        this.conferencia_id = conferencia_id;
    }

    @Override
    public String toString() {
        return "AsistentesVO{" + "cedula=" + cedula + ", nombre=" + nombre + ", apellido=" + apellido + ", conferencia_id=" + conferencia_id + ", correo=" + correo + '}';
    }
    
}
