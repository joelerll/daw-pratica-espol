/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author joelerll
 */
public class ConferenciaDAO {
    public  List<ConferenciaVO> getConferencias(){
        CallableStatement cs = null;
        List <ConferenciaVO> conferencias = new ArrayList();
        String sql = "SELECT * FROM conferencias";
//        System.out.println(sql);
        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                ConferenciaVO conferencia = new ConferenciaVO();
                conferencia.setId(rs.getInt(1));
                conferencia.setNombre(rs.getString(2));
                conferencia.setFecha(rs.getString(3));
                conferencia.setDescripcion(rs.getString(4));
                conferencias.add(conferencia);
                
            }
            conn.desconectar();
        } catch(SQLException ex) {
            System.out.println("model.ConferenciaDAO ERROR");
            System.out.println(ex);
            return null;
        }
        return conferencias;
    } 
    
    public void createConferencia(String nombre, String fecha, String descripcion) throws ParseException{
        String sql = "INSERT INTO conferencias (nombre,fecha,descripcion) VALUES (?,?,?)";
//        System.out.println(sql); 
        //Timestamp ts = Timestamp.valueOf(parseDate);
        //Timestamp ts = new Timestamp(System.currentTimeMillis());
        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, fecha);
            ps.setString(3, descripcion);
            ps.executeUpdate();
            conn.desconectar();
//            System.out.println("Producto guardado");
        } catch(SQLException ex) {
            System.out.println("no se pudo insertar conferencia");
        }
    }
    
    public ConferenciaVO getConferencia(int id){
        ConferenciaVO conferencia = new ConferenciaVO();
        String sql = "SELECT * FROM conferencias where id=" + id + ";";
        
        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                conferencia.setId(rs.getInt(1));
                conferencia.setNombre(rs.getString(2));
                conferencia.setFecha(rs.getString(3));
                conferencia.setDescripcion(rs.getString(4));         
            }
            conn.desconectar();
        } catch(SQLException ex) {
            System.out.println("model.ConferenciaDAO ERROR");
            return null;
        }
        
        return conferencia;
    }
    
    public void editConferencia(String id, String nombre, String fecha, String descripcion) {
        String sql = "UPDATE conferencias SET nombre = ?, fecha = ?, descripcion = ? WHERE id = ?;";
                
        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareCall(sql);
            ps.setString(1, nombre);
            ps.setString(2, fecha);
            ps.setString(3, descripcion);
            ps.setString(4, id);
            ps.executeUpdate();
            conn.desconectar();
        } catch(SQLException ex) {
            System.out.println("model.ConferenciaDAO ERROR");
            System.out.println(ex);
        }
    }
        
    public List<ConferenciaVO> mostarJSON(List<ConferenciaVO> conferencias_i){
        List<ConferenciaVO> conferencias= new ArrayList();
        for (ConferenciaVO conferencia : conferencias_i){
            ConferenciaVO conf = new ConferenciaVO();
            conf.setId(conferencia.getId());
            conf.setNombre(conferencia.getNombre());
            conferencias.add(conf);
        }
        return conferencias;
    }
    
    //Eliminar conferencia.
    public void deleteConferencia(String id) {
        //Para borrar, necesito conseguir el Id del elemento a borrar, pero este es un dato invisible PARA EL USUARIO
        //
        String sql = "DELETE FROM conferencias WHERE id = ?";
                
        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareCall(sql);
            ps.setString(1, id);
            ps.executeUpdate();
            conn.desconectar();
        } catch(SQLException ex) {
            System.out.println("model.ConferenciaDAO ERROR");
            System.out.println(ex);
        }
    }
}
