/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author joelerll
 */
public class AsistenteDAO {
    public  List<AsistenteVO> getAsistentes(){
        CallableStatement cs = null;
        List <AsistenteVO> asistentes = new ArrayList();
        String sql = "SELECT * FROM asistentes";
        System.out.println(sql);
        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                AsistenteVO asistente = new AsistenteVO();
                asistente.setCedula(rs.getString(1));
                asistente.setNombre(rs.getString(2));
                asistente.setApellido(rs.getString(3));
                asistente.setConferencia_id(rs.getInt(4));
                asistente.setCorreo(rs.getString(5));
                asistentes.add(asistente);
                
            }
            conn.desconectar();
        } catch(SQLException ex) {
            System.out.println("model.AsistenteDAO ERROR");
            return null;
        }
        return asistentes;

    }
    
    public boolean createAsistente(String cedula, String nombre, String apellido, String conferencia, String correo) {
        String sql = "INSERT INTO asistentes (cedula,nombre,apellido,conferencia,correo) VALUES (?,?,?,?,?)";

        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cedula);
            ps.setString(2, nombre);
            ps.setString(3, apellido);
            ps.setInt(4, Integer.parseInt(conferencia));
            ps.setString(5, correo);
            ps.execute();
            conn.desconectar();
        } catch(SQLException ex) {
            System.out.println("no se pudo insertar asistente");
            return false;
        }
        return true;

    } 
    
    public boolean deleteAsistente(String id){
        String sql = "DELETE FROM asistentes WHERE cedula = ?";  

        try{
            db conn = new db();
            PreparedStatement ps = conn.conectar().prepareCall(sql);
            ps.setString(1, id);
            ps.executeUpdate();
            conn.desconectar();
            return true;
        } catch(SQLException ex) {
            System.out.println("model.ConferenciaDAO ERROR");
            System.out.println(ex);
            return false;
        }

    }
}
