/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author joelerll
 */
public class db {
    public Connection conexion = null;
    private final String url="jdbc:mysql://localhost:3306/";
    private final String root="root";
    private final String pass="user";
    private final String db_name = "conferenciasdb";
    public Connection conectar() throws SQLException {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("----------Intentando conectar base de datos");
            conexion = DriverManager.getConnection(url+db_name,root,pass);
            if (conexion!=null) {
                
            }
        }catch (ClassNotFoundException ex) {
            System.out.println("---------No se pudo establecer la conexion");
        
            return null;
        }
        return conexion;
    }
    public void desconectar(){
        System.out.println("Desconectar db");
        conexion = null;
    }
}
