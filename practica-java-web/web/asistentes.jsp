<%-- 
    Document   : asistentes
    Created on : Jan 14, 2017, 9:58:34 PM
    Author     : joelerll
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Asistentes</title>
        <link rel="stylesheet" href="./public/bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="./public/css/asistentes.css"/>
        <link rel="stylesheet" href="./public/fontawesome/css/font-awesome.min.css"/>
    </head>
    <body ng-app="asistentesApp" ng-controller="MostarAsistentes">
        <%@ include file="./_partials/_nav.html" %> <br>
        <h1>Asistentes</h1>
          <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" >
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ingresar Asistente</h4>
              </div>
              <div class="modal-body">
                  <form action="" method="" id="modal-form">
                      <div class="form-group">
                        <label for="cedula" name="cedula">Cedula</label>
                        <input type="text" ng-model="asistente_ingresar.cedula"class="form-control" id="cedula" name="cedula" placeholder="cedula">
                      </div>
                      <div class="form-group">
                        <label for="nombre" name="nombre">Nombre</label>
                        <input type="text"ng-model="asistente_ingresar.nombre" class="form-control" id="nombre" name="nombre" placeholder="nombre">
                      </div>
                      <div class="form-group">
                        <label for="apellido" name="apellido">Apellido</label>
                        <input type="text" ng-model="asistente_ingresar.apellido" class="form-control" id="apellido" name="apellido" placeholder="apellido">
                      </div>
                      <div class="form-group">
                        <label for="correo" name="correo">Correo</label>
                        <input type="text" ng-model="asistente_ingresar.correo" class="form-control"  id="correo" name="correo" placeholder="correo"></input>
                      </div>
                      <div class="form-group" >
                          <select ng-model="conf" ng-options="conferencia.nombre for conferencia in conferencias track by conferencia.id">
                          </select>
                      </div>
                     
                      <div class="modal-footer" >
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary" data-dismiss="modal" ng-click="anadir()" >Registrar</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
         <button type="button" class="btn btn-primary btn-lg" id="registrar-button" data-toggle="modal" data-target="#myModal">
                  Registrar asistente
            </button>
         
          <div class="alert alert-danger alert-dismissible" id="msg-status" role="alert" hidden>
                <a class="close" onclick="$('.alert').hide()">×</a> 
                <label id="msg-status-text">dsdsdsdsdsd</label>
            </div>
          
        <div >

            
            <table class="table">
                <thead>
                    <tr>
                        <th>Cedula</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="asistente in asistentes">
                        <td>{{asistente.cedula}}</td>
                        <td>{{asistente.nombre}}</td>
                        <td>{{asistente.apellido}}</td>
                        <td>{{asistente.correo}}</td>
                        <td><a class="btn btn-danger" ng-click="getIndex(asistente.cedula)" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash-o fa-lg"></i></a></td>
                    </tr>
                </tbody>
            </table>
            
            <div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modalEliminar">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <p>Escoja la opcion</p>
                  </div>
                  <div class="modal-body">
                     <form action="" method="">
                        <button class="btn btn-danger" data-dismiss="modal" ng-click="borrar()">OK</button>
                        <button class="btn" data-dismiss="modal">Cancelar</button>
                    </form>
                  </div>
                    
                </div>
              </div>
            </div>
           
            
        </div>
          
        <script src="./public/jquery/jquery.min.js"></script>
        <script src="./public/bootstrap/js/bootstrap.min.js"></script>
        <script src="./public/angular/angular.min.js"></script>
        <script src="./public/bootstrap-ui/ui-bootstrap-tpls-2.4.0.min.js"></script>
        <script src="./app/app.js"></script>
    </body>
</html>
