<%-- 
    Document   : conferencias
    Created on : Jan 11, 2017, 1:37:56 PM
    Author     : joelerll
--%>

<%@page import="java.util.List"%>
<%@page import="models.ConferenciaDAO"%>
<%@page import="models.ConferenciaVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Espol - Conferencias</title>
        <link rel="stylesheet" href="./public/bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="./public/css/conferencias.css"/>
        <link rel="stylesheet" href="./public/fontawesome/css/font-awesome.min.css"/>
    </head>
    <body>
        <%@ include file="./_partials/_nav.html" %>
              <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Crear conferencia</h4>
              </div>
              <div class="modal-body">
                  <form action="Conferencias?action=create" method="POST" id="modal-form">
                      <div class="form-group">
                        <label for="nombre" name="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre de la conferencia">
                      </div>
                      <div class="form-group">
                        <label for="fecha" name="fecha">Fecha</label>
                        <input type="date" class="form-control" id="fecha" name="fecha">
                      </div>
                      <div class="form-group">
                        <label for="descripcion" name="descripcion">Descripción</label>
                        <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="¿Algo que agregar?"></textarea>
                      </div>
                      <input type="text" id="id-conferencia" name="id-conferencia" hidden>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btn-form-modal">Registrar</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <div class="contenido">
            <br>
            <h3>Conferencias a relizarse</h3>
            <button type="button" class="btn btn-primary btn-lg" id="registrar-button">
                  Registrar Nueva
            </button>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Descripcion</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%List<ConferenciaVO> conferencias = (List<ConferenciaVO>) request.getAttribute("conferencias");%>
                    <%int idEliminar = 0;%>
                    <% for(ConferenciaVO conferencia : conferencias){ %>
                
            <!-- Small modal -->
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modalEliminar">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <form id="confirmacion" action="Conferencias?action=delete" method="post">
                        <button class="btn btn-danger delete" type="submit" name="id">OK</button>
                        <button class="btn" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
              </div>
            </div>    
            <div class="modal fade bs-example-modal-sm <%=conferencia.getId()%>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modalEliminar">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                   
                    <form action="ConferenciaDelete" method="post">
                        <button class="btn btn-danger" type="submit" name="id" >OK</button>
                        <button class="btn" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
              </div>
            </div>
                    <tr>
                        <td><%=conferencia.getNombre()%></td>
                        <td><%=conferencia.getFecha()%></td>
                        <td><%=conferencia.getDescripcion()%></td>
                        <td><a href="#" class="editar-button" data-dataid="<%=conferencia.getId()%>"><i class="fa fa-edit fa-2x"></i></a></td>
                        <td><a class="btn btn-danger dang" href="#" data-toggle="modal" data-target="#modalEliminar" id=<%=conferencia.getId()%> ><i class="fa fa-trash-o fa-lg"></i></a></td>
                    </tr>
                <%}%>
                </tbody>
            </table>
            <h2>
            </h2>
           
        </div>

        <script src="./public/jquery/jquery.min.js"></script>
        <script src="./public/bootstrap/js/bootstrap.min.js"></script>
        <script src="./public/js/conferencias.js"></script>
    </body>
</html>
