angular.module("asistentesApp",['ui.bootstrap'])
  .controller('MostarAsistentes', function($http,$scope){
    $scope.nombre = "joel";
    $scope.asistentes = [];
    $scope.conferencias = [];
    $scope.asistente_ingresar = {};
    $scope.index_borrar = "";
    $http.get('/practica-java-web/api/ConferenciasREAD').then(function(response) {
        $scope.conferencias = response.data;
       // $scope.$apply()
    })
    $http.get('/practica-java-web/api/AsistentesREAD').then(function(response) {
        $scope.asistentes = response.data;
    })
    
    //borrar api
   /*
    $http({
            method: 'DELETE',
            url: '/roles/' + roleid,
            data: {
                user: userId
            },
            headers: {
                'Content-type': 'application/json;charset=utf-8'
            }
        })
        .then(function(response) {
            console.log(response.data);
        }, function(rejection) {
            console.log(rejection.data);
        });
    */
    $scope.conf = {};
    $scope.asistente_ingresar.cedula = "";
    $scope.asistente_ingresar.nombre = "";
    $scope.asistente_ingresar.apellido = "";
    $scope.asistente_ingresar.correo = "";
    
     //ingresar api
     /**/
    $scope.getIndex = function(id_asis) {
        $scope.index_borrar = id_asis;
        console.log($scope.index_borrar);
    };
    
    $scope.anadir = function() {

        var idConferencia = "";
        if(typeof $scope.conf.id === "undefined") 
            idConferencia = "";
        else
            idConferencia = $scope.conf.id;
        
        var dataObj = {
                        'cedula' : $scope.asistente_ingresar.cedula,
                        'nombre' : $scope.asistente_ingresar.nombre,
                        'apellido' : $scope.asistente_ingresar.apellido,
                        'idConferencia' : idConferencia.toString(),
                        'correo' : $scope.asistente_ingresar.correo
		};

        $http.post('/practica-java-web/api/AsistenteCREATE', dataObj)
            .then(
                function(response){
                  console.log(response.data);
                  console.log(JSON.stringify(response.data));
                  $http.get('/practica-java-web/api/AsistentesREAD').then(function(response) {
                        $scope.asistentes = response.data;
                    });
                    console.log(response.data.msg);
                  if (response.data.exito === "0") {
                      $("#msg-status").show();
                      $("#msg-status").removeClass().addClass("alert alert-danger alert-dismissible");
                      $("#msg-status-text").html(response.data.msg);
                  } else {
                      $("#msg-status").show();
                      $("#msg-status").removeClass().addClass("alert alert-success alert-dismissible");
                      $("#msg-status-text").html(response.data.msg);
                      $("#cedula").value("");
                      $("#nombre").value("");
                      $("#apellido").value("");
                      $("#correo").value("");
                  }
                }, 
                function(response){
                    console.log(response.data);
                    console.log(JSON.stringify(response.data));
                }
             );
    };
    
    $scope.borrar = function(ced){
        console.log($scope.index_borrar)
        cont = 0;
        $scope.asistentes.forEach(function(conf){
            if (conf.cedula === $scope.index_borrar) {
                $scope.asistentes.splice(cont,1);
                var data = $.param({
                    cedula: conf.cedula
                });
                console.log(data)
                $http.delete('api/AsistenteDELETE?' + data).then(
                    function(response){
                      $("#msg-status").show();
                      $("#msg-status").removeClass().addClass("alert alert-danger alert-dismissible");
                      $("#msg-status-text").html("¡Eliminado con éxito!");
                    }, 
                    function(response){
                      // failure call back
                    }
                 );
            }
            cont = cont + 1;
        })
    }
    
    
    
  })
  
  
// data-toggle="modal" data-target="#modalEliminar"  ng-click="borrar(asistente.cedula)"